# Ports services

- jenkins runs on port 8000
- sonarqube runs on port 9000
- portainer runs on port 9001
- nexus runs on port 8081

## Notes

- Wait a couple of seconds to test the sonarqube service
- Wait a couple of seconds until nexus services start up
- To test homework2 services install curl with the following command

  sudo apt install curl

  then use the following command to validate the get endpoint of the service

  curl localhost:5070
