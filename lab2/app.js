const dns = require('dns');
const {
  networkInterfaces
} = require('os');
var os = require("os");
const express = require('express');
const app = express();
const port = 5070;

function getLocalIpAddress() { 
    const nets = networkInterfaces();
    const results = Object.create(null);

    for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
        if (net.family === 'IPv4' && !net.internal) {
        if (!results[name]) {
            results[name] = [];
        }
        results[name].push(net.address);
        }
    }
    }
    return results;
}



function getDateTimeFormated() {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();
    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();

    return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
}
 

app.get('/', (req, res) => {
    let addresses = getLocalIpAddress();
    const eth0 = addresses.eth0;
    let result = {
        address: eth0,
        hostname: os.hostname(),
        time_zone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        dateTime: getDateTimeFormated()
    }
    console.log(result);
    res.send(result);
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});