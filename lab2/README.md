# Instructions  needed to create the image
## Docker ignore

* Add .dockerignore file in order to avoid the files

```
.git
*Dockerfile*
*docker-compose*
node_modules

```

## Build image

* command to build the image, in order to run the following command the current path sould be the root of project in this context the path of the project is lab2, the image name will have the name of the homework in this case should be homework2

```
docker build -t homework2 .
```

## Tag image and push the image

* Tg new image and then push it to docker.Should have a docker hub account and also had to enable a token security to login first before to run the the command to tag and push

### Command to login on to docker hub

```
docker login -u [username]
```
### Command to tag

```
docker image tag homework2 [username]/homework2:jorge.valdivia

```
### Command to push the image

```
docker image push [username]/homework2:jorge.valdivia

### Command to run the image

```
docker run -d -p 5070:5070 --name h2 jheorge/homework2:jorge.valdivia
```
### Test endpoint

```
curl localhost:5070

```