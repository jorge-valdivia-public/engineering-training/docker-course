#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo 'This script must be run as root \n Try to use sudo' 
   exit 1
fi
# Variables
POSTGRES_PASSWORD='postgres1234.'
POSTGRES_USER='adminp'
USER_DEFINED_NETWORK_NAME='h1'
POSTGRES_CONTAINER_NAME='postgres-h1'
SONARQUBE_CONTAINER_NAME='sonarqube-h1'
JENKINS_CONTAINER_NAME='jenkins-h1'
NEXUS3_CONTAINER_NAME='nexus3-h1'
PORTAINER_CONTAINER_NAME='portainer-h1'
declare -a listVolumes=("sonarqube-conf" "sonarqube-data" "sonarqube-logs" "sonarquebe-extensions" "jenkins-home" "portainer_data")
###

function clean_environment {
   rm -f *.log
   declare -a containers=("${POSTGRES_CONTAINER_NAME}" "${SONARQUBE_CONTAINER_NAME}" "${JENKINS_CONTAINER_NAME}" "${NEXUS3_CONTAINER_NAME}" "${PORTAINER_CONTAINER_NAME}")
   for container in ${containers[@]}; do
      docker rm -f ${container}
      echo "container => ${container} was deleted."
   done
   echo "############### CLEAN OPERATION FINISHED! ###############"
} 

function validate_max_map_count {
   count_matchs=`sudo sysctl -a | grep "vm.max_map_count = 262144" | wc -l` 
   echo "this is the count matchs variable ==> ${count_matchs}"
   if [[ "$count_matchs" != "1" ]]; then
   echo '      => set sysctl variables '
   echo '          => changing value of vm.max_map_count=262144 variables'
   sysctl -w vm.max_map_count=262144
   echo '** restart the docker service'
   systemctl restart docker
   echo '** docker restart finished'
   else
   echo 'the variable already set it up'
   fi
}

function init_script {
   echo '################### H1 ###################'
   echo '# author: Jorge Valdivia                 #'
   echo '# title: Homework 1 - Docker Containers  #'
   echo '# date: 07/06/2022                       #'
   echo '##########################################'
   echo ' ' 
   echo 'Step 1'
   echo '  * configurerequierements of sonarqube'
   validate_max_map_count
   echo 'Step 2'
   echo 'creating docker network h1'
   docker network create ${USER_DEFINED_NETWORK_NAME}
   echo 'Step 2'
   echo '  * installing postgres'
   echo "      => with password: ${POSTGRES_PASSWORD} "
   echo "logs on : ${POSTGRES_CONTAINER_NAME}_output.log & ${POSTGRES_CONTAINER_NAME}_error.log"
   docker run --network ${USER_DEFINED_NETWORK_NAME} --name ${POSTGRES_CONTAINER_NAME} -e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} -e POSTGRES_USER=${POSTGRES_USER} -d postgres 1> ${POSTGRES_CONTAINER_NAME}_output.log 2> ${POSTGRES_CONTAINER_NAME}_error.log &
   echo 'Step 3'
   echo '    * installing Sonarqube'
   echo '        ==> creating volumes'

   for volName in ${sqVolumes[@]}; do
      docker volume create $volName
      echo "volume ${volName} was created."
   done

   echo '        ==> create sonarqube container'
   echo "logs on : ${SONARQUBE_CONTAINER_NAME}_output.log & ${SONARQUBE_CONTAINER_NAME}_error.log"
   docker run --network ${USER_DEFINED_NETWORK_NAME} -d --name ${SONARQUBE_CONTAINER_NAME} -p 9000:9000 -p 9092:9092 -e SONARQUBE_JDBC_USERNAME=${POSTGRES_USER} -e SONARQUBE_JDBC_PASSWORD=${POSTGRES_PASSWORD} -e SONARQUBE_JDBC_URL=jdbc:postgresql://${POSTGRES_CONTAINER_NAME}:5432/postgres  -v sonarqube-conf:/opt/sonarqube/conf -v sonarqube-data:/opt/sonarqube/data -v sonarqube-logs:/opt/sonarqube/logs -v sonarqube-extensions:/opt/sonarqube/extensions sonarqube 1> ${SONARQUBE_CONTAINER_NAME}_output.log 2> ${SONARQUBE_CONTAINER_NAME}_error.log
   echo 'Step 4'
   echo '        * installing Jenkins'
   echo "logs on : ${JENKINS_CONTAINER_NAME}_output.log & ${JENKINS_CONTAINER_NAME}_error.log"
   docker run -d --name ${JENKINS_CONTAINER_NAME}  -p 8080:8080 -p 50000:50000 -v jenkins-home:/var/jenkins_home jenkins/jenkins 1> ${JENKINS_CONTAINER_NAME}_output.log 2> ${JENKINS_CONTAINER_NAME}_error.log 
   echo 'Step 5'
   echo '        * installing nexus3'
   echo "logs on : ${NEXUS3_CONTAINER_NAME}_output.log & ${NEXUS3_CONTAINER_NAME}_error.log"
   docker run -d -p 8081:8081 --name ${NEXUS3_CONTAINER_NAME} sonatype/nexus3 1> ${NEXUS3_CONTAINER_NAME}_output.log 2> ${NEXUS3_CONTAINER_NAME}_error.log
   echo 'Step 6'
   echo '        * installing Portainer'
   echo "logs on : ${PORTAINER_CONTAINER_NAME}_output.log & ${PORTAINER_CONTAINER_NAME}_error.log"
   docker run -d -p 8000:8000 -p 9443:9443 --name ${PORTAINER_CONTAINER_NAME} --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest 1> ${PORTAINER_CONTAINER_NAME}_output.log 2> ${PORTAINER_CONTAINER_NAME}_error.log
   echo "############### H1 FINISHED! ###############"

}

echo "************ select the operation *****************"
echo "************ Homework-1 Jorge Valdivia ************"
echo "  1)Init script"
echo "  2)Clean logs and remove containers"

read n
case $n in
  1) echo "You chose Option 1"
         init_script
  ;;
  2) echo "You chose Option 2"
         clean_environment
      ;;
  *) echo "invalid option";;
esac




